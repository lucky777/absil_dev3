# Packages to install to build the slides

(Tested on Linux Mint)
> - texlive-latex-base
> - texlive-latex-recommended
> - texlive-latex-extra
> - texlive-pictures
> - texlive-lang-french
> - texlive-science
> - texlive-pstricks
> - texlive-fonts-extra

    If you are lazy you can just install 'texlive-full'