#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

class fraction
{
	unsigned num, denom;
	bool positive;

	public:
		fraction(int num, int denom);
		fraction(unsigned num, unsigned denom, bool positive = true);				
		
        //member function: bad practice
        //fraction operator*(const fraction& f1) const; 
    
        //fraction operator*(const int& i) const;
    
        inline unsigned numerator() const { return num; }
        inline unsigned denominator() const { return denom; }
        inline bool sign() const { return positive; }
    
		string toString()
		{
			string str;
			if(! positive)
				str+="-";
			str += to_string(num);
            str+=" / ";
            str+= to_string(denom);
			return str;
		}
};

fraction operator*(const fraction& f1, const fraction& f2);

fraction::fraction(int num, int denom) 
	: num(abs(num)), denom(abs(denom)), positive((num >= 0 && denom >= 0) || (num <= 0 && denom <= 0))
{}

fraction::fraction(unsigned num, unsigned denom, bool positive) 
	: num(num), denom(denom), positive(positive)
{}

/*
fraction fraction::operator *(const fraction& f) const
{
	return fraction(num * f.num, denom * f.denom, 
		(positive && f.positive) || (!positive && !f.positive));
}
*/

/*
fraction fraction::operator*(const int& i) const
{
    return fraction(i * numerator(), denominator());
}
*/

fraction operator*(const fraction& f, const int& i)
{
     return fraction(i * f.numerator(), f.denominator());  
}

fraction operator*(const int& i, const fraction& f)
{
    return f * i;
}

fraction operator*(const fraction& f1, const fraction& f2)
{
	return fraction(f1.numerator() * f2.numerator(), 
                    f1.denominator() * f2.denominator(), 
		            (f1.sign() && f2.sign()) 
                        || (! f1.sign() && !f2.sign()));
}

int main()
{
	fraction f1 (2,3);// 2/3  //fraction f11(2,3,false);
	fraction f2 (-4,5); //- 4 / 5
	
	fraction f3 = f1 * f2;
	cout << f3.toString() << endl;
    
    fraction f4 = f1 * 2;
    cout << f4.toString() << endl;
    fraction f5 = 2 * f1;

	cout << (f1 * f2 * f3).toString() << endl; //((f1 * f2) * f3)
}
