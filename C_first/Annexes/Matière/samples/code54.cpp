#include <iostream>
#include <algorithm>

using namespace std;

struct Printer
{
    void operator()(int i) const { cout << i << " " << endl; }
};

int main()
{
    vector<int> v = {1, 2, 3, 4, 5};
    for_each(v.begin(), v.end(), Printer);
}
