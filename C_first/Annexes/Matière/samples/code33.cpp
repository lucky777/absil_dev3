#include <iostream>

void f(int) {std::cout << "int" << std::endl; }
void f(int&) {std::cout << "int&" << std::endl; }
void f(const int&) {std::cout << "c int&" << std::endl; }
void f(int&&) {std::cout << "int&&" << std::endl; }

int main()
{
    int i = 2; int & ri = i; const int ci = 3; const int & rci = ci;
    f(2); f(i); f(ri); f(ci); f(rci);
}
