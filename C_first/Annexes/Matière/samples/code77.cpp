#include <iostream>

template<class Container>
std::ostream& operator<<(std::ostream& out, const Container& c)
{
    for(const auto& e : c)
        out << e << " ";
    out << std::endl;
    return out;
}

int main()
{    
    std::cout << "Hello" << std::endl;
}
