#include <iostream>

template<int N>
struct A
{
    A() { std::cout << N << std::endl; } 
};

int main()
{
    int i = 2;
    A<i> a;
}
