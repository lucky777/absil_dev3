#include <iostream>

using namespace std;

class Array
{
    int* a;
    unsigned s;
    public:
        Array(unsigned s) : a(new int[s]), s(s) {}
        ~Array() { delete[] a; }
        int& operator[](unsigned k) { return a[k]; }
};

void f(Array a) {}

int main()
{
    Array a(5); 
    for(unsigned i = 0; i < 5; i++)
        a[i] = i;
    Array b(6); a = b;
    for(unsigned i = 0; i < 6; i++)
        cout << a[i] << " ";
    cout << endl;
}
