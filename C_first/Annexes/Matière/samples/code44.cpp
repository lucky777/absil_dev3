#include <iostream>

using namespace std;

struct A
{
    ~A() = delete;  
};

void f(A a) {}

int main()
{
    A a;
    f(a);
}
