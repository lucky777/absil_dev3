#include <iostream>

struct A
{
    virtual void f() = 0;  
};

struct B : A
{
    void f() override { std::cout << "Hello" << std::endl; }
};

int main()
{
    B b;
    A a = b;
    a.f();
}
