#include "code46-b.h

struct A
{
    int _i;
    B* _b;
    
    inline int i() const
    {
        return _i;   
    }
};