#include <stdio.h>

void swap(int * i, int * j) 
{
	int * tmp = i;
	i = j; j = tmp;
}

int main() 
{
	int i = 2; int j = 3; swap(&i, &j);
	printf("%d %d\n", i, j);
}