#include <iostream>

using namespace std;

struct Integer
{
    int i;    
    Integer(int i) : i(i) {}
    Integer operator+(const Integer& i) const { return Integer(this->i + i.i); }
        
};

int main()
{
    Integer i = 2;
    cout << (3 + (i + 2)).i << endl;
}
