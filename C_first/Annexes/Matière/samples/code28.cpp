#include <iostream>
#include <cstring>

const char* s1 = "Hello";
constexpr unsigned len = std::strlen(s1);

int main()
{
    std::cout << len << std::endl;
}
