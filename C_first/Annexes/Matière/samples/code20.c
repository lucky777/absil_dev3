#include <stdio.h>

void print(void* pt) 
{
	printf("%d\n", (int)*pt);
}

int main() 
{
	int i = 2;
	print(&i);
}