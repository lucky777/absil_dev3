#include <iostream>

int main()
{
    int c = 3;
    
    auto f = [](int a, int b)
    {
        return a * b * c;
    };
    
    decltype(f) g = f;
    std::cout << g(3,4) << std::endl;
}
