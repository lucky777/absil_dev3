#include <iostream>

template<class ... Args>
void print_content(const Args* ... args)
{    
    ((std::cout << " " << *args), ...);
}

int main()
{
    int i = 2; double f = 3.1; double * pt = &i;
    print_content(&i, &f, pt);
}
