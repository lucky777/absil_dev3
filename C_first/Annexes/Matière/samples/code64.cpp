#include <iostream>

using namespace std;

struct A {}
struct B {
    ~B() { cout << "-B"; }
};

int main()
{
    A * a = new B;
    delete a;
}
