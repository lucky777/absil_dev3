#include <iostream>

struct A 
{
    void print() { std::cout << "A" << std::endl; }
};

struct B : A 
{
    void print() { std::cout << "B" << std::endl; }
};

int main()
{
    B b; A& a = b;
    a.print();
}
