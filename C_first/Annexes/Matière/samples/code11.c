#include <stdio.h>

int min(const int tab[], int n) 
{
	int min;
	for(int i = 0; i < n; i++)
		if(min > tab[i])
			min = tab[i];	
	return min;
}

int main() 
{
	const int tab[] = {4, 3, 1, 8, 5, 9, 10};
	printf("%d\n", min(tab, 7));
}