#include <iostream>

template<class Function, class... Params>
auto apply(Function&& f, Params&& ... params) -> decltype(f(params...))
{
    return f(std::forward<Params&&>(params)...);
}

void f(int& i)
{
    i++;
}

void f(const int& i)
{    
}

int main()
{
    int i = 0;
    apply(f, i);
    std::cout << i << std::endl;
}
