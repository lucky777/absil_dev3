#include <iostream>
#include <vector>
#include <list>

using namespace std;

template<class Container>
ostream& operator<<(ostream& out, const Container& c)
{
    for(const auto& e : c)
        out << e; //I can't add << " "
    out << endl;
    return out;
}

int main()
{
    vector<int> v = {1,2,3,4};
    list<int> l = {1,2,3,4};    
    cout << v << endl; //ok
    cout << l << endl; //ok
    //cout << "Hello" << endl; //ko
    //cout << std::string("Hello") << endl; //ko
}
