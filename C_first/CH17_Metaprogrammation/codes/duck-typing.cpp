#include <iostream>
#include <concepts>

using namespace std;

template<class T>
concept Duck = requires(T t)
{
    {t.quack() };  
};

template<Duck D>
void squish(D d)
{
    d.quack();   
}

struct Goose
{
    void quack() { cout << "Quack!" << endl; }  
};

struct Cat
{
    void meow() { cout << "Meow!" << endl; }  
};

int main()
{
    Goose g;
    squish(g);
    
    Cat c;
    squish(c);
}
