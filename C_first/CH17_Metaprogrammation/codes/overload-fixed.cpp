#include <iostream>
#include <vector>
#include <list>

using namespace std;

template<class Container>
concept NotPrintable = ! requires(std::ostream out, Container c)
{    
    { operator<<(out, c) };
};

template<NotPrintable Container>
ostream& operator<<(ostream& out, const Container& c )
{
    for(const auto& e : c)
        out << e << " "; 
    out << endl;
    return out;
}

int main()
{
    vector<int> v = {1,2,3,4};
    list<int> l = {1,2,3,4};    
    cout << v << endl; //ok
    cout << l << endl; //ok
    cout << "Hello" << endl; //ok
    cout << std::string("Hello") << endl; //ok
}
