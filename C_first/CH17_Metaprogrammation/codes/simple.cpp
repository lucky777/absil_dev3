#include <iostream>
#include <concepts>

using namespace std;

/*
template<class T>
T gcd(const T& a, const T& b)
{
    if(b == 0)
        return a;
    return gcd(b, a % b);
}
*/

/*
template<std::integral T>
T gcd(const T& a, const T& b)
{
    if(b == 0)
        return a;
    return gcd(b, a % b);
}
*/
std::integral auto gcd(std::integral auto a, std::integral auto b)
{
    if(b == 0)
        return a;
    return gcd(b, a % b);
    //return b == 0 ? a : gcd(b, a % b); //ko : use of auto before deduction of return type
}

int main()
{
    cout << gcd(27, 15) << endl;
    
    //struct A {};
    //cout << gcd(A(), A()) << endl; //ko, obscure message without concepts
}
