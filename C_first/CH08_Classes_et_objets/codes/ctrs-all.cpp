#include <iostream>
#include <initializer_list>

using namespace std;

struct A
{
    long int i;            
};

struct B
{
    int i;
    B(int i) : i(i) {}
};

struct C
{
    int i;
    C(const initializer_list<int>& l) {}
};

struct D
{
    int i;
    D(int) { cout << "C" << endl; }
    D(const initializer_list<int>& l) { cout << "L" << endl; }
};

int main()
{
    A a;
    //A aa(2); //no cstr
    A aaa{2};
    //A aaaa{2.1}; //no narrowing conv
    //A aaaaa = {2.1}; //no narrowing conv
    
    //B b; //no def-cstr
    B bb = 2; //ok : conv (cf. conv chapter)
    B bbb(2); //ok : call B(int)
    B bbbb{2}; //ok
    B bbbbb = {2}; //ok
    //B bbbbbb {2.1}; //no narrowing conv
    //B bbbbbbb = {2.1}; //no narrowing conv
    
    //C c; //no def-cstr
    //C cc(2); //no matching cstr
    //C ccc = 2; //no matching conv
    C cccc {2};
    C ccccc = {2};
    
    //D d; //no def-cstr
    D dd(2); //ok
    D ddd = 2; //ok : conv
    D dddd {2};
    D ddddd = {2};    
}
