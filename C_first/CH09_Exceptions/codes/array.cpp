#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

class array
{
	int n;
	double * tab;
	
	public:
		array(int nbr) : n(nbr), tab(new double[n]) {}
		
		array(const array & v) = delete;
        //I should do something for = ...
		
		~array()
		{
            if(tab != nullptr) //good practice
			    delete[] tab;
		}	
				
		double& get (int i)
		{
			rangeCheck(i);
			return tab[i];
		}

		inline int size() const
		{
			return n;
		}

	private:
		inline void rangeCheck(int i) const
		{
			if(i < 0 || i >= n)
			{
				string s = "out of range : size of ";
				s += to_string(n);
				s += " , accessed at ";
				s += to_string(i);                
				throw out_of_range(s);
			}
		}
};

int main()
{
	array v(5);

	for(int i = 0; i < 5; i++)
		v.get(i) = i * i;

	for(int i = 0; i < 5; i++)
		cout << v.get(i) << endl;
	cout << endl;

	v.get(0) = 2; //try to remove & from []

	for(int i = 0; i < 5; i++)
		cout << v.get(i) << endl;
	
	try
	{
		v.get(-1) = 4; //try to remove rangeCheck
	}
	catch(const out_of_range& e)
	{
		cerr << e.what() << endl;
	}
}
